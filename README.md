# CoBox

**CoBox** is an encrypted p2p file system and distributed back-up tool.

CoBox was developed using funding from the EU Ledger NGI programme.

Its aim is to facilitate a transition to a sovereign commons-based data infrastructure and a co-operative distributed cloud architecture.

CoBox uses the hypercore protocol and the hyperswarm distributed hash table (DHT) to connect peers in a distributed network. All data is encrypted on the client, then replicated across a swarm of peers.

## Getting Started

```
# Clone the repo recursively on either http or ssh
git clone --recursive http://ledger-git.dyne.org:3000/CoBox/cobox-core.git

# install dev dependencies
yarn

# start cobox-server
yarn start

# start cobox-server dev server
yarn dev:server

# use the cli
yarn cli
```

### About

Coops often rely on proprietary services for managing their data, which do not reflect their cooperative principles or business practices.

CoBox is an innovative suite of open hardware and software providing accessible governance tools for organisations and networks. It comprises a human-centric plug and play server with pre-installed privacy enhancing coop software designed to promote members' data sovereignty. CoBox seeks to build on the historic tendency for networks of coops to collaborate.

CoBox brings a cooperative approach to hosting, treating data as a common good owned by citizens. Leveraging the benefits of self-hosting combined with peer-to-peer technologies to share responsibilities of maintaining data availability.

As well as serving internal organisational governance needs these simple to use tools provide the infrastructure for governance of the CoBox network, to manage, research and guide the ecosystem, providing a state of the art alternative to corporate models.

### Resources

* [MVP TRL 4-5 Submission](https://cobox.cloud/mvp.pdf)
* [Demo Video](https://cobox.cloud/cli-demo-video.webm)
